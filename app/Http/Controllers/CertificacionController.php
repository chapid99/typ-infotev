<?php

namespace App\Http\Controllers;

use App\Models\certificacion;
use App\Http\Controllers\Controller;
use App\Models\usuarios;
use Illuminate\Http\Request;

class CertificacionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // ->join('certificacion','tipovehiculos.id','=','certificacion.tipovehiculo_id')
        // ->join('certificacion','tipotransportes.id','=','certificacion.tipotransporte_id')
        $cert = certificacion::join('usuarios','usuarios.id','=','certificacions.user_id')->select('*')->simplePaginate(20);
        // dd("cert",$cert);
        return view('home.index',['cert' => $cert]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\certificacion  $certificacion
     * @return \Illuminate\Http\Response
     */
    public function show(certificacion $certificacion)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\certificacion  $certificacion
     * @return \Illuminate\Http\Response
     */
    public function edit(certificacion $certificacion)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\certificacion  $certificacion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, certificacion $certificacion)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\certificacion  $certificacion
     * @return \Illuminate\Http\Response
     */
    public function destroy(certificacion $certificacion)
    {
        //
    }
}
