<?php

namespace App\Http\Controllers;

use App\Models\tipovehiculo;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TipovehiculoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\tipovehiculo  $tipovehiculo
     * @return \Illuminate\Http\Response
     */
    public function show(tipovehiculo $tipovehiculo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\tipovehiculo  $tipovehiculo
     * @return \Illuminate\Http\Response
     */
    public function edit(tipovehiculo $tipovehiculo)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\tipovehiculo  $tipovehiculo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, tipovehiculo $tipovehiculo)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\tipovehiculo  $tipovehiculo
     * @return \Illuminate\Http\Response
     */
    public function destroy(tipovehiculo $tipovehiculo)
    {
        //
    }
}
