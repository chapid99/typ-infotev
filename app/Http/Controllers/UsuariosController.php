<?php

namespace App\Http\Controllers;

use App\Models\usuarios;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class UsuariosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $docu = $request->documento;
        if(empty(trim($docu))){
            $usuarios = usuarios::select('*')->get();
            return view('usuario.index',['usuarios' => $usuarios]);
        }
        else{
            $usuarios = usuarios::select('*')->where('documento','like', '%' .$request->documento. '%')->orWhere('nombre','like', '%' .$request->documento. '%')->simplePaginate(20);
            return view('usuario.index',['usuarios' => $usuarios]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('usuario.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $usuario = new usuarios();
        $usuario->nombre = $request->nombre;
        $usuario->documento = $request->documento;
        $usuario->lugarexpedicion = $request->lugarexpedicion;
        $usuario->telefono = $request->telefono;
        $existDocument = usuarios::where('documento',$request->documento)->count();
        if($existDocument==0){
            $usuario->save();
            return redirect('/usuarios')->with('msj' ,'Usuario creado correctamente');
        }
        else{
            return redirect('/usuarios')->with('errormsj' ,'Error, el documento ya existe');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\usuarios  $usuarios
     * @return \Illuminate\Http\Response
     */
    public function show(usuarios $usuarios)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\usuarios  $usuarios
     * @return \Illuminate\Http\Response
     */
    public function edit(usuarios $user)
    {
        // $user = usuarios::find($id);
        return view('usuario.edit',compact('user'));
        // return view('usuario.edit',['user' => $user[0]]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\usuarios  $usuarios
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, usuarios $user)
    {
        if($user->documento == $request->documento){
            $existDocument = 0;
        }
        else{    

            $existDocument = usuarios::where('documento',$request->documento)->count();
            
        }
        $user->nombre = $request->nombre;
        $user->documento = $request->documento;
        $user->lugarexpedicion = $request->lugarexpedicion;
        $user->telefono = $request->telefono;
        if($existDocument==0){
            if($user->save()){
                return redirect()->route('usuarios')->with('msj' ,'Usuario actualizado correctamente');
            }
            else{
                return redirect()->route('usuarios')->with('msj' ,'Error actualizando');
            }
        }
        else{
            return redirect()->route('usuarios')->with('errormsj' ,'Error, el documento ya existe');
        }       

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\usuarios  $usuarios
     * @return \Illuminate\Http\Response
     */
    public function destroy(usuarios $usuarios)
    {
        //
    }
}