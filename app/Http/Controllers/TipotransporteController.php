<?php

namespace App\Http\Controllers;

use App\Models\tipotransporte;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TipotransporteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\tipotransporte  $tipotransporte
     * @return \Illuminate\Http\Response
     */
    public function show(tipotransporte $tipotransporte)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\tipotransporte  $tipotransporte
     * @return \Illuminate\Http\Response
     */
    public function edit(tipotransporte $tipotransporte)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\tipotransporte  $tipotransporte
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, tipotransporte $tipotransporte)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\tipotransporte  $tipotransporte
     * @return \Illuminate\Http\Response
     */
    public function destroy(tipotransporte $tipotransporte)
    {
        //
    }
}
