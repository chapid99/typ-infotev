<div class="row">

    <div class="col-md-12">
        <div class="col-sm-12 portlets">

            <div class="widget">
                <div class="widget-header transparent">
                    <h2><strong>Buscar por </strong> documento o por nombre</h2>
                    <div class="additional-btn">
                        <a href="#" class="hidden reload"><i class="icon-ccw-1"></i></a>
                        <a href="#" class="widget-toggle"><i class="icon-down-open-2"></i></a>
                        <a href="#" class="widget-close"><i class="icon-cancel-3"></i></a>
                    </div>
                </div>
                <div class="widget-content padding">
                    <div id="basic-form">
                        <form action="/usuarios" method="get">
                            <div class="form-group">
                                <label for="exampleInputPassword1"><b>Documento</b></label>
                                <input type="text" name="documento" class="form-control">
                            </div>

                            <input type="submit" name="" value="Buscar" class="btn btn-info">
                        </form>
                    </div>
                </div>
            </div>

        </div>


    </div>
</div>