@extends("layouts.admin");

@section("content")

<div class="content-page">
    <div class="content">
        <div class="page-heading" style="text-align: center;">
            <h1><i class='fa fa-table'></i>Bienvenido</h1>
            <h3>Ingresa la informacion correspondiente</h3>
        </div>
        <div class="row">

            <div class="col-md-12">
                <div class="col-sm-12 portlets">

                    <div class="widget">
                        <div class="widget-header transparent">
                            <h2><strong>Crear</strong> Informacion</h2>
                            <div class="additional-btn">
                                <a href="#" class="hidden reload"><i class="icon-ccw-1"></i></a>
                                <a href="#" class="widget-toggle"><i class="icon-down-open-2"></i></a>
                                <a href="#" class="widget-close"><i class="icon-cancel-3"></i></a>
                            </div>
                        </div>
                        <div class="widget-content padding">
                            <div id="basic-form">
                                <form action="{{route('usuarios.update', $user)}}" method="post">
                                    @csrf

                                    @method('put')

                                    <div class="form-group">
                                        <label for="exampleInputPassword1"><b>Nombre completo</b></label>
                                        <input type="text" name="nombre" id="nombre" require class="form-control"
                                            value="{{$user->nombre}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1"><b>Documento</b></label>
                                        <input type="text" name="documento" id="documento" require class="form-control"
                                            value="{{$user->documento}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1"><b>Lugar de expedicion</b></label>
                                        <input type="text" name="lugarexpedicion" id="lugarexpedicion" require
                                            value="{{$user->lugarexpedicion}}" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1"><b>Telefono</b></label>
                                        <input type="text" name="telefono" id="telefono" require class="form-control"
                                            value="{{$user->telefono}}">
                                    </div>
                                    <input type="submit" name="" value="Confirmar" class="btn btn-info">
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection