@extends("layouts.admin");

@section("content")


<div class="content-page">
    <br><br>
    <!-- @if($errors->any())
        <div class="alert alert-danger nomargin">
            @foreach($errors->all() as $error)
            <a href="#" class="alert-link">Error: {{$error}}</a>.
            @endforeach
        </div>
        @endif -->
    <div class="content">
        <div class="page-heading">
            <h1><i class='fa fa-bell'></i> Usuarios</h1>
            <h3>Todos los usuarios</h3>
        </div>
        @include('usuario.search')
        <div class="floating">
            <a href="{{url('usuarios/create')}}" class="btn btn-primary btn-fab">
                <i class="material-icons">Crear usuario</i>
            </a>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="widget">
                    <div class="widget-header">
                        <h2><strong>Seccion de</strong> Usuarios</h2>
                        <div class="additional-btn">
                            <a href="#" class="hidden reload"><i class="icon-ccw-1"></i></a>
                            <a href="#" class="widget-toggle"><i class="icon-down-open-2"></i></a>
                            <a href="#" class="widget-close"><i class="icon-cancel-3"></i></a>
                        </div>
                    </div>
                    <div class="widget-content">
                        <br>
                        <div class="table-responsive">
                            <form class='form-horizontal' role='form'>
                                <table id="datatables-1" class="table table-striped table-bordered" cellspacing="0"
                                    width="100%">
                                    <thead>
                                        <tr>
                                            <th>Nombre completo</th>
                                            <th>Documento</th>
                                            <th>Telefono</th>
                                            <th>Editar</th>
                                            <th>Agregar certificado</th>
                                        </tr>
                                    </thead>


                                    <tbody>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td>
                                                <div class="btn-group btn-group-xs">
                                                    <form action="" method="delete">
                                                        <input type="hidden"
                                                            class="btn btn-link red-text no-padding no-margin"
                                                            value="Actualizar Pagina">

                                                    </form>
                                                </div>
                                            </td>
                                        </tr>
                                        @foreach($usuarios as $usuarioss)

                                        <tr>
                                            <td>{{$usuarioss->nombre}}</td>
                                            <td>{{$usuarioss->documento}}</td>
                                            <td>{{$usuarioss->telefono}}</td>
                                            <td>
                                                <div class="btn-group btn-group-xs">
                                                    <a href="{{route('usuarios.edit',$usuarioss->id)}}"
                                                        data-toggle="tooltip" title="Editar" class="btn btn-default"><i
                                                            class="fa fa-edit"></i> Editar</a>
                                            </td>
                                            <td>
                                                <form action="/certific" method="post">
                                                    <input type="hidden" name="id" value="{{$usuarioss->id}}">
                                                    <input type="submit" name="" value="Agregar certificado"
                                                        class="btn btn-info">
                                                </form>

                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div>
        </div>
    </div>
</div>
@if(session()->has('msj'))
<script>
var msj = "{{ session('msj') }}";
swal("Correcto", msj, "success");
</script>
@endif
@if(session()->has('errormsj'))
<script>
var errormsj = "{{ session('errormsj') }}";
swal("Opss", errormsj, "error");
</script>
@endif

@endsection