@extends("layouts.admin");

@section("content")

<div class="content-page">
    <br><br>
    <center>
        @if(session()->has('msj'))
        <div class="alert alert-success">
            <a href="#" class="alert-link">{{ session('msj') }}</a>.
        </div>
        @endif
        @if(session()->has('errormsj'))
        <div class="alert alert-danger nomargin">
            <a href="#" class="alert-link">{{ session('errormsj') }}</a>.
        </div>

        @endif
    </center>
    <div class="content">
        <div class="page-heading">
            <h1><i class='fa fa-bell'></i> Certificacion</h1>
            <h3>Cursos tecnicas y pericia</h3>
        </div>


        <div class="row">

            <div class="col-md-12">
                <div class="widget">
                    <div class="widget-header">
                        <h2><strong>Seccion de</strong> certificado</h2>
                        <div class="additional-btn">
                            <a href="#" class="hidden reload"><i class="icon-ccw-1"></i></a>
                            <a href="#" class="widget-toggle"><i class="icon-down-open-2"></i></a>
                            <a href="#" class="widget-close"><i class="icon-cancel-3"></i></a>
                        </div>
                    </div>
                    <div class="widget-content">
                        <br>
                        <div class="table-responsive">
                            <form class='form-horizontal' role='form'>
                                <table id="datatables-1" class="table table-striped table-bordered" cellspacing="0"
                                    width="100%">
                                    <thead>
                                        <tr>
                                            <th>Fecha</th>
                                            <th>Nombre completo</th>
                                            <th>Documento</th>
                                            <th>Editar</th>
                                            <th>Ver</th>
                                            <th>Eliminar</th>
                                        </tr>
                                    </thead>


                                    <tbody>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td>
                                                <div class="btn-group btn-group-xs">
                                                    <form method="post" class="inline-block">
                                                        <input type="hidden"
                                                            class="btn btn-link red-text no-padding no-margin"
                                                            value="Actualizar Pagina">

                                                    </form>
                                                </div>
                                            </td>
                                        </tr>
                                        @foreach($cert as $certs)

                                        <tr>
                                            <td>{{$certs->fecha}}</td>
                                            <td>{{$certs->nombre}}</td>
                                            <td>{{$certs->documento}}</td>
                                            <td>
                                                <div class="btn-group btn-group-xs">
                                                    <a href="{{url('/certificacion/'.$certs->id.'/edit')}}"
                                                        data-toggle="tooltip" title="Editar" class="btn btn-default"><i
                                                            class="fa fa-edit"></i> Editar</a>
                                            </td>
                                            <td>
                                                <a href="{{url('/vercertificacion/'.$certs->id)}}" title="Editar"
                                                    class="btn btn-info"><i class="fa fa-show"></i> Ver</a>

                                            </td>
                                            <td>
                                            test
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div>
        </div>
    </div>

</div>
@endsection