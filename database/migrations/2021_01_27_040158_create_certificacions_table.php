<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCertificacionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('certificacions', function (Blueprint $table) {
            $table->id();
            $table->string('lugar');
            $table->string('fecha');
            $table->string('estado');
            $table->string('evaluacionpsi');
            $table->string('puntajeconocimientos');
            $table->string('puntajetyp');
            $table->string('puntajefinal');
            $table->string('observaciones');
            $table->string('recomendaciones');
            $table->string('codigo');
            $table->string('cnl');
            $table->string('tipocert');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('tipovehiculo_id');
            $table->unsignedBigInteger('tipotransporte_id');
            $table->foreign("user_id")->references("id")->on("usuarios");
            $table->foreign("tipovehiculo_id")->references("id")->on("tipovehiculos");
            $table->foreign("tipotransporte_id")->references("id")->on("tipotransportes");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('certificacions');
    }
}
